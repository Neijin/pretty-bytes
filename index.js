'use strict';

const UNITS = [
	'B',
	'KB',
	'MB',
	'GB',
	'TB',
	'PB',
	'EB',
	'ZB',
	'YB'
];

const MAX_UNIT_FOR_NO_FIXED = 'KB';
const MAX_FIXED = 2;

function Exponent(bytes)
{
	return Math.min(Math.floor(Math.log10(bytes) / 3), UNITS.length - 1);
}

function Convert(bytes, requestedUnit) 
{
	if (!Number.isFinite(bytes)) {
		throw new TypeError(`Expected a finite bytes number, got ${typeof bytes}: ${bytes}`);
	}

	let exponent = requestedUnit !== undefined ? UNITS.indexOf(requestedUnit.toUpperCase()) : Exponent(bytes);
	let formatedBytes = Number((bytes / Math.pow(1000, exponent)).toFixed(exponent <= UNITS.indexOf(MAX_UNIT_FOR_NO_FIXED) ? 0 : MAX_FIXED));
	let unit = UNITS[exponent];

	return {'value' : formatedBytes, 'unit' : unit};
};
module.exports.Convert = Convert

function BestUnitFor(bytes1, bytes2) 
{
	if (!Number.isFinite(bytes1)) {
		throw new TypeError(`Expected a finite first bytes number, got ${typeof bytes1}: ${bytes1}`);
	}
	if (!Number.isFinite(bytes2)) {
		throw new TypeError(`Expected a finite second bytes number, got ${typeof bytes2}: ${bytes2}`);
	}

	let exponent1 = Exponent(bytes1);
	let exponent2 = Exponent(bytes2);

	return 	UNITS[Math.min(exponent1, exponent2)];
};
module.exports.BestUnitFor = BestUnitFor
